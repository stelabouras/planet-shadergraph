# Planet Shader Graph

![screenshot](https://media.giphy.com/media/d55CabgRNPlOBbaZ75/giphy.gif)

That's a simple Unity 2019.3.3f1 project that uses the Universal Rendering Pipeline and the Shader Graph to replicate the planet shader effect as seen at [Simon Trumpler's post](https://simonschreibt.de/gat/diablo-3-resource-bubbles/#update8) and recreated in code by [Ben Golus](https://twitter.com/bgolus/status/1233279164947451909) and in Unreal Engine 4 Material Editor by [Bruno Afonseca](https://twitter.com/3dBrunoVFX/status/1233289304526508032).

The third shader that supports two layers of clouds was inspired by [Klemen's tweet](https://twitter.com/klemen_lozar/status/1233904296149667840).

Disclaimer: That's my first experiment with Shader Graph, so don't take the graphs implemented in this project at face value, they can definitely be optimized and improved further!
