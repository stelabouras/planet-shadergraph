﻿#ifndef FBMINCLUDE_INCLUDED
#define FBMINCLUDE_INCLUDED

// http://www.iquilezles.org/www/articles/warp/warp.htm
// http://www.iquilezles.org/www/articles/fbm/fbm.htm
// https://www.shadertoy.com/view/4s23zz
// https://www.shadertoy.com/view/lsl3RH
// https://www.shadertoy.com/view/4s23zz
float random (in float2 st) {
    return frac(sin(dot(st.xy,
                         float2(12.9898,78.233)))
                 * 43758.5453123);
}

float noise (in float2 st) {
    float2 i = floor(st);
    float2 f = frac(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    float2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners percentages
    return lerp(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

void fbm_float(float2 x, in float H, int numOctaves, out float t)
{    
    float G = exp2(-H);
    float f = 1.0;
    float a = 1.0;
    t = 0.0;
    for( int i=0; i<numOctaves; i++ )
    {
        t += a*noise(f*x);
        f *= 2.0;
        a *= G;
    }
}

#endif //FBMINCLUDE_INCLUDED