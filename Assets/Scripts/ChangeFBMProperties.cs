﻿using UnityEngine;

public class ChangeFBMProperties : MonoBehaviour
{
    private Material _material;

    [SerializeField]
    PlanetColors[] planetColors;
    private int currentPlanetIndex = 0;

    private void Start()
    {
        _material = GetComponent<Renderer>().material;

        UpdateColors(currentPlanetIndex);
    }

    public void CycleColors()
    {
        currentPlanetIndex += 1;

        if (currentPlanetIndex >= planetColors.Length)
            currentPlanetIndex = 0;

        UpdateColors(currentPlanetIndex);
    }

    private void UpdateColors(int planetIndex)
    {
        if (planetIndex < 0 || planetIndex >= planetColors.Length)
            return;

        PlanetColors colors = planetColors[planetIndex];
        _material.SetColor("_Color1", colors.color1);
        _material.SetColor("_Color2", colors.color2);
        _material.SetColor("_Color3", colors.color3);
        _material.SetColor("_Color4", colors.color4);
        _material.SetColor("_Color5", colors.color5);
    }

    public void UpdateNumOctaves(float numOctaves)
    {
        _material.SetFloat("_NumOctaves", numOctaves);
    }

    public void UpdateH(float h)
    {
        _material.SetFloat("_H", h);
    }

    public void UpdateTimeFactor(float timeFactor)
    {
        _material.SetFloat("_TimeFactor", timeFactor);
    }

    public void UpdatePanSpeed(float panSpeed)
    {
        _material.SetFloat("_PanSpeed", panSpeed);
    }

    public void UpdateSpherify(float spherify)
    {
        _material.SetFloat("_Spherify", spherify);
    }
}
