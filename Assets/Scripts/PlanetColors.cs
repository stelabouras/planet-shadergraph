﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlanetColors : ScriptableObject
{
    public Color color1;
    public Color color2;
    public Color color3;
    public Color color4;
    public Color color5;
}
